server {
    listen             5000;
    server_name        localhost;
    absolute_redirect  off;
    root               /usr/share/nginx/html;
    index              index.html;

    location / {
        rewrite ^/accessibility-audits/accessibility-audits$ /accessibility/audits permanent;
        rewrite ^/accessibility-audits/keyboard-only$ /accessibility/keyboard-only permanent;
        rewrite ^/accessibility-audits/screen-readers$ /accessibility/screen-readers permanent;
        rewrite ^/accessibility-audits/semantics-and-content$ /accessibility/semantics-and-content permanent;
        rewrite ^/accessibility-audits/visual$ /accessibility/visual permanent;
        rewrite ^/components/form$ /patterns/forms permanent;
        rewrite ^/components/form/code$ /patterns/forms/code permanent;
        rewrite ^/components/form/contribute$ /patterns/forms/contribute permanent;
        rewrite ^/content/date-time$ /content/date-and-time permanent;
        rewrite ^/content/error-messages$ /patterns/forms permanent;
        rewrite ^/content/voice-tone$ /content/voice-and-tone permanent;
        rewrite ^/get-started/contribute$ /get-started/contributing permanent;
        rewrite ^/get-started/structure$ /get-started/navigating-pajamas permanent;
        rewrite ^/layout/grid$ /product-foundations/layout permanent;
        rewrite ^/layout/layers$ /product-foundations/elevation permanent;
        rewrite ^/layout/responsive-first$ /product-foundations/layout permanent;
        rewrite ^/layout/spacing$ /product-foundations/spacing permanent;
        rewrite ^/product-foundations/colors$ /product-foundations/color permanent;
        rewrite ^/product-foundations/interaction$ /usability/affordance permanent;
        rewrite ^/product-foundations/saving-and-feedback$ /usability/saving-and-feedback permanent;
        rewrite ^/regions/empty-states$ /patterns/empty-states permanent;
        rewrite ^/regions/empty-states/code$ /patterns/empty-states/code permanent;
        rewrite ^/regions/filtering$ /patterns/filtering permanent;
        rewrite ^/regions/merge-request-reports$ /patterns/merge-request-reports permanent;
        rewrite ^/regions/navigation$ /patterns/navigation permanent;
        rewrite ^/regions/search$ /patterns/searching permanent;
        rewrite ^/regions/settings$ /usability/settings-management permanent;
        rewrite ^/resources/design-resources$ /get-started/design-resources permanent;
        rewrite ^/usability/error-prevention$ /usability/destructive-actions permanent;
        rewrite ^/usability/helping-users$ /usability/contextual-help permanent;
        rewrite ^/usability/i18n$ /content/i18n permanent;
        rewrite ^/usability/onboarding-users$ /usability/onboarding permanent;
        rewrite ^/vpat/508$ /accessibility/508 permanent;
        rewrite ^/vpat/wcag$ /accessibility/wcag permanent;

        try_files $uri $uri/index.html $uri/ =404;
    }

    error_page 500 502 503 504 /error.html;
    error_page 404 /404.html;
}
