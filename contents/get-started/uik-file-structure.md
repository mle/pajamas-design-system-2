---
name: File structure
---

The Pajamas UI Kit is comprised of multiple Figma files that organize separate, but related concepts. Each publishes a library of styles and components that can be enabled in your design files.

- [**Component library**](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Component-library): The main file for design components that are used in Pajamas and in all other files. The library is published as "📙 Component library" and enabled for all team design files.
- [**Typescale - Documentation Markdown**](https://www.figma.com/file/V3HKN83B7rf2T6sseLMrxa/Typescale-Documentation-Markdown): ⚠️ **This file is scheduled for deprecation in 16.3.**
- [**Typescale - Compact Markdown**](https://www.figma.com/file/mjAZxHkK95TlQ6L14aNp2M/Typescale-Compact-Markdown): ⚠️ **This file is scheduled for deprecation in 16.3.**
- [**Data Visualization**](https://www.figma.com/file/17NxNEMa7i28Is8sMetO2H/Data-Visualization): Components, styles, and charts used within GitLab. Published as the "Data Visualization" library.
- [**Product pages**](https://www.figma.com/file/tzpLCamRZNr2tTPwCP2UY4/Product-Pages): Components, layouts, regions, and page templates used within GitLab. Items herein are not globally used throughout the product and not included in the main component library. Published as the "Product Pages" library.
- [**GitLab Product Icons**](https://www.figma.com/file/h4YjjttHL5YI0mXZfQ4uuU/GitLab-Product-Icons): The main file for product iconography. The library is published as "GitLab Product Icons" and enabled for all team files.

In addition to the links above, files are available from the [GitLab Product Design](https://www.figma.com/@GitLabDesign) community page and the [project repository](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/tree/main/ui-kit).

## Fonts

The UI kit files make use of GitLab Sans, and GitLab Mono. Refer to the [type fundamentals](/product-foundations/type-fundamentals) for more information. The fonts are available to download in [this package](https://www.npmjs.com/package/@gitlab/fonts).

## Plugins

It may change in the future, but at the moment we don’t use plugins for critical actions or capabilities to avoid making any part of the design process reliant on plugin updates or functionality. Rather, we believe that each user should determine which plugins to use for their own workflow.
